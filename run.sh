#!/bin/bash

#PBS -l select=1:ncpus=4:ompthreads=4
#PBS -l walltime=00:00:10
#PBS -m n
#PBS -o out-omp-demo.txt
#PBS -e err-omp-demo.txt

if [[ -n $PBS_O_WORKDIR ]]; then
  cd $PBS_O_WORKDIR || :
fi

echo "Using $OMP_NUM_THREADS threads"

for executable in ./bin/demo ./bin/omp-demo; do
  echo "Running $executable"
  echo "$((time $executable) 2>&1)"
done
