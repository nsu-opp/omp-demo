#!/bin/bash
set -x

mkdir -p bin
cd bin || exit 1
cmake ..
make all
cd ..
