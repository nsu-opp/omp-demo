#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>


#define N_ITER 10000
#define PI  3.1415926535897932385

#define ITER_DELAY_US (4 * 1000 * 1000 / N_ITER)


int main() {
    double sum = 0;
    const double step = 2 * PI / N_ITER;

#pragma omp parallel for reduction (+:sum)
    for (int i = 0; i < N_ITER; i += 1) {
        sum += sin(i * step);
        usleep(ITER_DELAY_US);
    }

    printf("%lf\n", sum);

    return EXIT_SUCCESS;
}
