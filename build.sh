#!/bin/bash
set -x

mkdir -p bin

gcc -std=gnu99 -O3 -Wno-unknown-pragmas -Wpedantic -Wall -Werror -Wextra -o ./bin/demo main.c -lm
gcc -std=gnu99 -O3 -Wpedantic -Wall -Werror -Wextra -o ./bin/omp-demo main.c -fopenmp -lm
gcc -std=gnu99 -O3 -Wpedantic -Wall -Werror -Wextra -o ./bin/err-omp-demo err.c -fopenmp -lm
